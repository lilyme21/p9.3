//////////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 10/1/19
// This program creates an array of 7 different objects called
// appointments and asks the user for a date. If the user provides
// an acceptable date, the program prints all the appointments
// scheduled for that date. It loops until user no longer wants to
// check a date.
//////////////////////////////////////////////////////////////////

import java.util.Scanner;                   // importing package to use Scanner

public class P9_3 {
    // creating superclass Appointment
    public static class Appointment {
        // initializing variables
        protected String reason;            // reason to hold description of appointment
        protected int[] date = new int[3];  // date to hold date of appointment
        // empty constructor
        public Appointment() {
        }
        // method to take user input to set the reason
        protected void setReason() {
            // prompting user to enter reason for appointment
            System.out.println("Reason for appointment: ");
            // setting reason to user input
            Scanner r = new Scanner(System.in);
            reason = r.nextLine();
        }
        // method to take user input to set the date
        protected void setDate() {
            // prompting user to enter date of appointment
            System.out.println("Date of appointment: ");
            // setting date to user input
            Scanner d = new Scanner(System.in);
            // converting user input to int array
            String[] strDates = d.nextLine().split("/");
            for (int i = 0; i < 3; i++) {
                date[i] = Integer.parseInt(strDates[i]);
            }
        }
        // method to print appointment and check boolean occursOn
        public String getReason(int year, int month, int day) {
            if(occursOn(year, month, day)) {
                return (reason);
            }
            else
                return " ";
        }
        // method to check if appointment is on the same date
        public boolean occursOn(int year, int month, int day) {
            return (year == date[2] && month == date[0] && day == date[1]);
        }
    }
    // creating a subclass OneTime that inherits from Appointment
    public static class OneTime extends Appointment {
        // constructor that takes user input
        public OneTime() {
            setReason();        // calling method from Appointment class
            setDate();          // calling method from Appointment class
        }
        // set constructor
        public OneTime(String r, String d) {
            // setting reason and date
            reason = r;
            String[] strDates = d.split("/");
            for (int i = 0; i < 3; i++) {
                date[i] = Integer.parseInt(strDates[i]);
            }
        }
    }
    // creating a subclass Daily that inherits from Appointment
    public static class Daily extends Appointment {
        // constructor that takes user input
        public Daily() {
            setReason();        // calling method from Appointment class
        }
        // set constructor
        public Daily(String r) {
            // setting reason
            reason = r;
        }
        // overwriting method occursOn
        public boolean occursOn(int year, int month, int day) {
            // always true because appointment is daily
            return true;
        }
    }
    // creating a subclass Monthly that inherits from Appointment
    public static class Monthly extends Appointment {
        // constructor that takes user input
        public Monthly() {
            setReason();        // calling method form Appointment class
            setDate();          // calling overwritten method
        }
        // set constructor
        public Monthly(String r, int d) {
            reason = r;         // setting reason to r
            date[0] = 0;        // setting month to 0 because it is every month
            date[1] = d;        // setting day to d
            date[2] = 0;        // setting year to 0
        }
        // overwriting method setDate
        protected void setDate() {
            // prompting user to enter a day for the appointment
            System.out.println("Day of appointment: ");
            // setting day in array date to user input
            Scanner d = new Scanner(System.in);
            date[1] = d.nextInt();
        }
        // overwriting method occursOn
        public boolean occursOn(int year, int month, int day) {
            // checking that the day is the same as the appointment
            return (day == date[1]);
        }
    }
    public static void main(String[] args) {
        // creating 7 different types of appointments
        OneTime app1 = new OneTime("Haircut", "09/10/19");
        Monthly app2 = new Monthly("Dentist", 4);
        Daily app3 = new Daily("Eat breakfast");
        OneTime app4 = new OneTime("Concert", "2/4/19");
        Monthly app5 = new Monthly("Wash bedding", 10);
        Monthly app6 = new Monthly("Pay bills", 4);
        Daily app7 = new Daily("Feed the dog");
        // creating array of appointments
        Appointment[] allAppointments = {app1, app2, app3, app4, app5, app6, app7};
        // initializing boolean check
        boolean check = true;
        // creating do loop to check appointments
        do {
            // prompting user to ender a date to check for appointments
            System.out.println("Enter a date (00/00/00) to see the appointments: ");
            // setting variable userDate to user input
            Scanner in = new Scanner(System.in);
            String[] userDate = in.nextLine().split("/");
            // searching through appointments and printing the appointments that match
            // the users date
            for (int i = 0; i < allAppointments.length; i++) {
                System.out.println(allAppointments[i].getReason(Integer.parseInt(userDate[2]),
                        Integer.parseInt(userDate[0]), Integer.parseInt(userDate[1])));
            }
            // asking user if they would like to continue checking dates
            // y if yes and n if no
            System.out.println("Check another date (y/n): ");
            // setting variable cont to user response
            Scanner c = new Scanner(System.in);
            Character cont = c.next().charAt(0);
            switch (cont) {
                case 'y':           // if yes loop continues
                    break;
                    case 'n':       // if no check is set to false and loop breaks
                        check = false;
                        break;
                default:            // if neither yes or no check is set to false and loop breaks
                    System.out.println("Unexpected value: " + cont + "\nTake that as a no.");
                    check = false;
                    break;
            }
        } while (check);
    }
}
